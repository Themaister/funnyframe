package com.eit.nidarosdomen;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.eit.nidarosdomen.funnyframe.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;

interface SurfaceTextureListener {
	public void surfaceCreated(SurfaceTexture texture);
}

class FunnyGLSurfaceView extends GLSurfaceView {
	FunnyFrame funny;
	
	FunnyGLSurfaceView(FunnyFrame funny) {
		super(funny);
		this.funny = funny;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		Log.i("FunnyView", "Touch event!");
		funny.takePhoto();
		return true;
	}
}

class FunnyFrameRenderer implements GLSurfaceView.Renderer, OnFrameAvailableListener {
	private Context ctx;
	private SurfaceTexture mTexture;
	private SurfaceTextureListener mFrame;
	private int[] glTexture;
	private int overlayTex;
	private int mProg;
	
	// Extension.
	private static final int GL_TEXTURE_EXTERNAL_OES = 0x8d65;

	private FloatBuffer mVertBuffer;
	private FloatBuffer mTexBuffer;
	private float[] matrixTexture = new float[16];
	private Boolean updateSurface = true;

	public FunnyFrameRenderer(SurfaceTextureListener frame, Context ctx) {
		super();
		mFrame = frame;
		this.ctx = ctx;
	}

	@Override
	public void onDrawFrame(GL10 arg0) {
		synchronized(updateSurface) {
			if (updateSurface) {
				mTexture.updateTexImage();
				mTexture.getTransformMatrix(matrixTexture);
				updateSurface = false;
			}
		}
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, glTexture[0]);
		GLES20.glTexParameterf(GL_TEXTURE_EXTERNAL_OES,
				GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameterf(GL_TEXTURE_EXTERNAL_OES,
				GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GL_TEXTURE_EXTERNAL_OES,
				GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GL_TEXTURE_EXTERNAL_OES,
				GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
		
		GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, overlayTex);

		GLES20.glUseProgram(mProg);
		
		int uSampler = GLES20.glGetUniformLocation(mProg, "sampler");
		GLES20.glUniform1i(uSampler, 0);
		int uOverlay = GLES20.glGetUniformLocation(mProg, "oSampler");
		GLES20.glUniform1i(uOverlay, 1);

		int aVert = GLES20.glGetAttribLocation(mProg, "aVertex");
		GLES20.glVertexAttribPointer(aVert, 4, GLES20.GL_FLOAT, false, 0, mVertBuffer);
		GLES20.glEnableVertexAttribArray(aVert);
		
		int aTex = GLES20.glGetAttribLocation(mProg, "aTexture");
		GLES20.glVertexAttribPointer(aTex, 4, GLES20.GL_FLOAT, false, 0, mTexBuffer);
		GLES20.glEnableVertexAttribArray(aTex);
		
		int mTex = GLES20.glGetUniformLocation(mProg, "mTexture");
		GLES20.glUniformMatrix4fv(mTex, 1, false, matrixTexture, 0);

		GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
	}

	@Override
	public void onSurfaceChanged(GL10 arg0, int width, int height) {
		GLES20.glViewport(0, 0, width, height);
	}

	private void createVert() {
		final int vertElems = 4 * 4;
		final int FLOAT_SIZE_BYTES = 4;

		final float[] verts = { 
				-1.0f, -1.0f, 0.0f, 1.0f, // Vertex
				-1.0f,  1.0f, 0.0f, 1.0f, 
				 1.0f, -1.0f, 0.0f, 1.0f, 
				 1.0f,  1.0f, 0.0f, 1.0f,
		};
		
		final float[] texCoords = {
				 0.0f,  0.0f, 0.0f, 1.0f, // Texture
				 0.0f,  1.0f, 0.0f, 1.0f, 
				 1.0f,  0.0f, 0.0f, 1.0f, 
				 1.0f,  1.0f, 0.0f, 1.0f, 
		};

		mVertBuffer = ByteBuffer.allocateDirect(vertElems * FLOAT_SIZE_BYTES)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		mTexBuffer = ByteBuffer.allocateDirect(vertElems * FLOAT_SIZE_BYTES)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();

		mVertBuffer.put(verts).position(0);
		mTexBuffer.put(texCoords).position(0);
	}

	private boolean compileShader(int prog, int shaderType, final String source) {
		int shader = GLES20.glCreateShader(shaderType);
		GLES20.glShaderSource(shader, source);
		GLES20.glCompileShader(shader);

		int[] compiled = new int[1];
		GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);

		if (compiled[0] != 0) {
			GLES20.glAttachShader(prog, shader);
			return true;
		} else {
			Log.e("GLRenderer", "Failed to compile shader: " + shaderType + " : "
					+ GLES20.glGetShaderInfoLog(shader));
			return false;
		}
	}

	private void createProgram() {
		mProg = GLES20.glCreateProgram();

		final String vertShader = 
				"attribute vec4 aVertex;\n" +
				"attribute vec4 aTexture;\n" +
				"varying vec2 vTexture;\n" +
				"varying vec2 vTextureOverlay;\n" +
				"uniform mat4 mTexture;\n" +
				"void main() {\n" +
				"  gl_Position = aVertex;\n" +
				"  vTextureOverlay = aTexture.xy;" +
				"  vTexture = (mTexture * aTexture).xy;\n" +
				"}";
		final String fragShader = 
				"#extension GL_OES_EGL_image_external : require\n" +
				"precision mediump float;\n" +
				"uniform samplerExternalOES sampler;" +
				"uniform sampler2D oSampler;" +
				"varying vec2 vTexture;\n" +
				"varying vec2 vTextureOverlay;\n" +
				"void main() {\n" +
				"  vec4 overlayColor = texture2D(oSampler, vTextureOverlay);\n" +
				"  gl_FragColor = mix(texture2D(sampler, vTexture), overlayColor, overlayColor.a);\n" +
				"}";

		compileShader(mProg, GLES20.GL_VERTEX_SHADER, vertShader);
		compileShader(mProg, GLES20.GL_FRAGMENT_SHADER, fragShader);
		GLES20.glLinkProgram(mProg);
	}
	
	private int loadTexture(int res) {
		int[] tmp = new int[1];
		GLES20.glGenTextures(1, tmp, 0);
		int tex = tmp[0];
		
		Matrix flip = new Matrix();
		flip.postScale(1.0f, -1.0f);
		
		Bitmap temp = BitmapFactory.decodeResource(ctx.getResources(), res);
		Bitmap bmp = Bitmap.createBitmap(temp, 0, 0, temp.getWidth(),
				temp.getHeight(), flip, true);
		temp.recycle();
		
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tex);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, 
				GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, 
				GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, 
				GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, 
				GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
				
		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bmp, 0);
		bmp.recycle();
		return tex;
	}

	@Override
	public void onSurfaceCreated(GL10 arg0, EGLConfig arg1) {
		// TODO Auto-generated method stub
		GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		GLES20.glDisable(GLES20.GL_DEPTH_TEST);
		glTexture = new int[1];
		GLES20.glGenTextures(1, glTexture, 0);
		mTexture = new SurfaceTexture(glTexture[0]);
		mFrame.surfaceCreated(mTexture);
		
		overlayTex = loadTexture(R.drawable.arch);
		
		mTexture.setOnFrameAvailableListener(this);

		createProgram();
		createVert();
	}

	@Override
	public synchronized void onFrameAvailable(SurfaceTexture texture) {
		synchronized (updateSurface) {
			updateSurface = true;
		}
	}
}

public class FunnyFrame extends Activity implements SurfaceTextureListener {

	private GLSurfaceView mView;
	volatile private Camera mCamera;
	private SurfaceTexture mTexture;
	private boolean mCameraBlocked = false;

	static private final String TAG = "FunnyFrame";

	@Override
	public void surfaceCreated(SurfaceTexture texture) {
		mTexture = texture;
		Thread thread = new Thread(new Runnable() {
			public void run() {
				startCamera(mTexture);
			}
		});
		thread.start();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (mTexture != null) {
			surfaceCreated(mTexture);
		}
	}

	private synchronized void startCamera(SurfaceTexture texture) {
		if (mCamera != null)
			return;

		mCamera = Camera.open();
		if (mCamera != null) {
			//mCamera.setDisplayOrientation(90); // Portrait mode.
			
			Camera.Parameters params = mCamera.getParameters();
			params.setSceneMode(Camera.Parameters.SCENE_MODE_PORTRAIT);
			mCamera.setParameters(params);

			try {
				mCamera.setPreviewTexture(texture);
			} catch (IOException e) {
				e.printStackTrace();
			}

			mCamera.setErrorCallback(new Camera.ErrorCallback() {
				@Override
				public void onError(int error, Camera camera) {
					Log.e(TAG, "Camera error #" + error + "!");
				}
			});

			mCamera.startPreview();
		} else {
			Log.e(TAG, "Failed to create Camera! :(");
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		if (mCamera != null) {
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
	}
	
	@SuppressLint("SimpleDateFormat")
	private void processPhoto(byte[] jpeg) {
		Log.i(TAG, "Decoding JPEG ...");
		
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inMutable = true;
		opts.inSampleSize = 2;
		Bitmap map = BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length, opts);
		Log.i(TAG, "Decoded JPEG (" + map.getWidth() + ", " + map.getHeight()
				+ ") ...");

		Canvas canvas = new Canvas(map);
		Drawable temp = getResources().getDrawable(R.drawable.arch);
		temp.setBounds(0, 0, map.getWidth(), map.getHeight());
		temp.draw(canvas);

		try {
			String date = new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
			OutputStream stream = new FileOutputStream(new File(Environment
					.getExternalStorageDirectory().getPath(),
					"DCIM/Camera/FunnyFrame-" + date + ".jpg"));
			map.compress(Bitmap.CompressFormat.JPEG, 70, stream);
		} catch (IOException e) {
			Log.e(TAG, "Failed to save image: " + e.getMessage());
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "Illegal arg: " + e.getMessage());
		}

		map.recycle();
	}
	
	public void takePhoto() {
		if (mCamera == null)
			return;
		
		if (mCameraBlocked)
			return;
		
		mCameraBlocked = true;
		mCamera.autoFocus(new Camera.AutoFocusCallback() {
			@Override
			public void onAutoFocus(boolean success, Camera camera) {
				Log.i(TAG, "Auto-focus " + (success ? "suceeded!" : "failed!"));
				camera.takePicture(new Camera.ShutterCallback() {
					@Override
					public void onShutter() {
						Log.i(TAG, "Shutter!");
					}
				}, new Camera.PictureCallback() {
					@Override
					public void onPictureTaken(byte[] data, Camera camera) {
						// Never works :(
					}
				}, new Camera.PictureCallback() {
					@Override
					public void onPictureTaken(byte[] data, Camera camera) {
						if (data != null)
							Log.i(TAG, "Picture taken (JPEG): " + data.length + " bytes!");
						else
							Log.e(TAG, "Data is null!");
						mCameraBlocked = false;
						
						final byte[] dataHold = data;
						Thread thread = new Thread(new Runnable() {
							public void run() {
								processPhoto(dataHold);
							}
						});
						thread.start();
						camera.startPreview();
					}
				});
			}
		});
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mView = new FunnyGLSurfaceView(this);
		mView.setEGLContextClientVersion(2);
		mView.setRenderer(new FunnyFrameRenderer(this, this.getBaseContext()));

		setTitle("FunnyFrame!");
		setContentView(mView);
	}
}
